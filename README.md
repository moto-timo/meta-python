# meta-python

Stand alone layer intended to be the home of python modules for OpenEmbedded. Any dependencies on layers outside of openembedded-core are handled with dynamic-layers. Work in progress.